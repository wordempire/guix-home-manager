;;; Guix Home Manager.
;;;
;;; Copyright © 2020 Jelle Licht <jlicht@fsfe.org>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
(define-module (home emacs)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (guix build utils)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix records)
  #:use-module (home profile)
  #:use-module (home utils)
  #:use-module (home)
  #:use-module (ice-9 match)
  #:use-module (language elisp parser)  ; for read-elisp
  #:use-module (srfi srfi-1)
  #:export (use-package-configuration
            use-package-configuration-package
            use-package-configuration-name
            use-package-configuration-defer
            use-package-configuration-after
            use-package-configuration-diminish
            use-package-configuration-general
            use-package-configuration-init
            use-package-configuration-config

            deferred-emacs-package

            emacs-init-configuration
            emacs-init-configuration-emacs
            emacs-init-configuration-init-file-name
            emacs-init-configuration-preamble
            emacs-init-configuration-use-packages

            elisp-file 
            enable-elisp-reader
            emacs-home-type))

(define (strip-emacs-name p)
  (let ((name (package-name p)))
    (string->symbol
     (if (string-prefix? "emacs-" name)
         (string-drop name 6)
         name))))

(define (enable-elisp-reader extender chr)
  (extender chr (lambda (chr port) (read-elisp port))))

(define (use-package-setup verbose? diminish? general?)
  `((eval-when-compile
     (require 'package)

     (setq package-archives nil
           package-enable-at-startup nil
           package--init-file-ensured t)

     (require 'use-package)

     ;; To help fix issues during startup.
     (setq use-package-verbose ,(if verbose? 't 'nil)))
    ,@(if diminish?
          '((require 'diminish))
          '())
    ,@(if general?
          '((require 'general))
          '())))

(define-record-type* <use-package-configuration>
  use-package-configuration make-use-package-configuration
  use-package-configuration?
  (package use-package-configuration-package
           (default #f))                ; <package>
  (name use-package-configuration-name
        (default (strip-emacs-name (use-package-configuration-package this-record)))
        (thunked))                      ; thunked<string>
  (defer use-package-configuration-defer (default #f))       ; bool | num
  (after use-package-configuration-after (default #f))       ; s-expr
  (diminish use-package-configuration-diminish (default #f)) ; bool | s-expr | symbol | string
  (general use-package-configuration-general (default #f))   ; bool | s-expr | symbol | string
  (init use-package-configuration-init (default #f))         ; #f | sexp* | sexp
  (config use-package-configuration-config (default #f))     ; #f | sexp* | sexp
  (custom use-package-configuration-custom (default #f)))    ; #f | sexp* | sexp

(define (deferred-emacs-package package)
  (use-package-configuration
   (package package)
   (defer #t)))

(define-record-type* <emacs-init-configuration>
  emacs-init-configuration make-emacs-init-configuration
  emacs-init-configuration?
  (emacs emacs-init-configuration-emacs
         (default emacs))
  (init-file-name emacs-init-configuration-init-file-name
                  (default ".config/emacs/init.el"))
  (preamble emacs-init-configuration-preamble
            (default #f))
  (use-packages emacs-init-configuration-use-packages
                (default #f)))

(define (use-package-configuration->elisp-sexp config*)
  (match config*
    (($ <use-package-configuration> package name defer after diminish general init config custom)
     `(use-package ,(name config*)
       ,@(match defer
           (#f '())
           (#t '(:defer t))
           ((? number? n)
            `(:defer ,n)))
       ,@(match after
           (#f '())
           ((? list? s-expression)
            `(:after ,s-expression)))
       ,@(match diminish
           (#f '())
           (#t '(:diminish))
           ((? symbol? _)
            `(:diminish ,diminish))
           ((? string? _)
            `(:diminish ,diminish))
           ((? list? _)
            `(:diminish ,diminish)))
       ,@(match general
           (#f '())
           (((? list? s-expressions) ...)
            `(:general ,@s-expressions))
           ((? list? s-expression)
            `(:general ,s-expression)))
       ,@(match init
           (#f '())
           (((? list? s-expressions) ...)
            `(:init ,@s-expressions))
           ((? list? s-expression)
            `(:init ,s-expression)))
       ,@(match config
           (#f '())
           (((? list? s-expressions) ...)
            `(:config ,@s-expressions))
           ((? list? s-expression)
            `(:config ,s-expression)))
       ,@(match custom
           (#f '())
           (((? list? s-expressions) ...)
            `(:custom ,@s-expressions))
           ((? list? s-expression)
            `(:custom ,s-expression)))))))

(define (wrap1list lst)
  (match lst
    (((_ _ ...) ...)
     lst)                               ; already list-of-lists
    ((_ _ ...)
     (list lst))))

(define (pseudo-elisp-file name expressions)
  (scheme-file name
               (concatenate (map wrap1list expressions))
                #:set-load-path? #f #:splice? #t))

(use-modules (gnu packages base))
(define (pseudo-elisp->elisp name mygexp)
  (computed-file name
                 #~(begin
                     (use-modules (ice-9 match)
                                  (srfi srfi-1))
                     (define (elisp-write in-list? exp . port)
                       (match exp
                         (('#{`}# rest)
                          (format (car port) "`")
                          (elisp-write in-list? rest (car port)))
                         (('#{,}# rest)
                          (format (car port) ",")
                          (elisp-write in-list? rest (car port)))
                         (('#{,@}# rest)
                          (format (car port) ",@")
                          (elisp-write in-list? rest (car port)))
                         (#(vs ...)
                          (format (car port) "[")
                          (elisp-write #t vs (car port))
                          (format (car port) "]"))
                         ('(%set-lexical-binding-mode #f) 'skip)
                         ((e ...)
                          (when (not in-list?) (format (car port) "("))
                          (unless (null? e)
                            (elisp-write #f (car e) (car port))
                            (for-each (lambda (v)
                                        (format (car port) " ")
                                        (elisp-write #f v (car port))) (cdr e)))
                          (when (not in-list?) (format (car port) ")")))
                         ((and (? pair?) (? dotted-list? l))
                          (format (car port) "(")
                          (elisp-write #t (drop-right l 0) (car port))
                          (format (car port) " . ")
                          (elisp-write #t (take-right l 0) (car port))
                          (format (car port) ")"))
                         (_ (apply write exp port))))
                     (call-with-output-file #$output
                       (lambda (out-port)
                         (call-with-input-file #$mygexp
                           (lambda (in-port)
                             (let loop ((form (read in-port)))
                               (if (not (eof-object?
                                         form))
                                   (begin
                                     (elisp-write #f form out-port)
                                     (format out-port "\n")
                                     (loop (read in-port))))))))))))

(define (elisp-file name . expressions)
  (pseudo-elisp->elisp
   name
   (pseudo-elisp-file (string-append "pseudo-" name) expressions)))

(define emacs-home-type
  (home-type
   (name 'emacs)
   (extension-points
    (list (home-extension-point
           (compose concatenate)        ; list of use-package-configurations
           (extend (lambda (config use-packages)
                     (emacs-init-configuration
                      (inherit config)
                      (use-packages (apply append (emacs-init-configuration-use-packages config)
                                           use-packages))))))))
   (extensions
    (list
     (home-extension
      (target root-home-type)
      (compute
       (lambda (config)
         (match config
           (($ <emacs-init-configuration> _ init-file-name preamble use-packages)
            (let* ((diminish? (any use-package-configuration-diminish use-packages))
                   (general? (any use-package-configuration-general use-packages))
                   (pseudo-init-file (pseudo-elisp-file
                                      "pseudo-init.pel"
                                      `(,@(if preamble
                                              preamble
                                              '())
                                        ,@(if (not (null? use-packages))
                                              (list (use-package-setup #f diminish? general?))
                                              '())
                                        ,@(map use-package-configuration->elisp-sexp
                                               use-packages))))
                   (elisp-init-file (pseudo-elisp->elisp "init-generated.el"
                                                       pseudo-init-file)))
              `((,init-file-name ,elisp-init-file))))))))
     (home-extension
      (target package-profile-home-type)
      (compute (match-lambda
                 (($ <emacs-init-configuration> configured-emacs _  __ use-packages)
                  (let ((diminish? (any use-package-configuration-diminish use-packages))
                        (general? (any use-package-configuration-general use-packages)))
                    (delete-duplicates
                     `(,@(if (not (null? use-packages))
                             (list emacs-use-package)
                             '())
                       ,@(if diminish?
                             (list emacs-diminish)
                             '())
                       ,@(if general?
                             (list emacs-general)
                             '())
                       ,configured-emacs
                       ,@(filter identity ; remove #f
                                 (map use-package-configuration-package use-packages)))))))))))
   (default-value (emacs-init-configuration))
   (description "The emacs home type")))
