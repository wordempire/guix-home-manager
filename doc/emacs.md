Emacs
====

Emacs is an extensible editor. This lets you manage the installation and
configuration of packages that extend Emacs and Emacs itself.

Main Configuration
------------------

Emacs is configured by using the `emacs-home-type` service type.

**Scheme Variable**: emacs-home-type

The type of service that generates configuration files and install packages
for Emacs.  Its value is an emacs-init-configuration object.

**Data Type**: emacs-init-configuration

Data type that represents the Emacs configuration and installed packages.
This data type has the following fields:

* **emacs** (default *emacs*): the emacs package to install.
* **init-file-name** (default ".config/emacs/init.el"): the location of the
  generated configuration file.
* **preamble** (default #f): the elisp code that is added to the start of your
  configuration file. This is a list of elisp s-expressions.
* **use-packages** (default '()): the configured Emacs packages that should be
  added to the generated configuration file. This is a list of
  `use-package-configuration` objects. Additionally, this list is also the
  extension point of this home-service.
  
**Data Type**: use-package-configuration

Data type that represents the an Emacs package and its configuration.  This
data type has the following fields:

* **package** (default #f): The (guix) package that should be made available
  in the profile. Use #f for built-in Emacs packages.
* **name**: The name used by Emacs to `require` the package; defaults to the
  **package** with any `emacs-` prefix dropped. This is a string.
* **defer** (default #f): Have Emacs lazily load this package. This is a boolean.
* **after** (default #f): Only have Emacs load this package after loading a
  list of other packages. This is a list of symbols.
* **diminish** (default #f): Rewrite any modeline indicators for this
  package. This is a string or symbol.
* **general** (default #f): Additional keybindings for a package. This is an
  s-expression or a list of s-expressions.
* **init** (default #f): Initializer snippet of elisp that is run before
  loading the package. This is either an s-expression or list of
  s-expressions.
* **config** (default #f): Configuration snippet of elisp that is run after
  loading the package. This is either an s-expression or list of
  s-expressions.
* **custom** (default #f): A mapping of variables and values. This is a either
  a single `(KEY VALUE-EXPRESSION)` or a list of these.
  
Utility functions
-----------------

Any time elisp snippets or s-expressions are mentioned, we need to make sure
that these are actually parsed as such by guile. The correct way to do this is
to use Guile's built-in elisp reader.

**Scheme Procedure**: (enable-elisp-reader extender chr) 

Call _extender_ to register the elisp reader as a reader extension prefixed by
_chr_. Typically, _extender_ is Guile's `read-hash-extend`, although
guile-reader's confined `read-hash-extend` also works.

**Scheme Procedure**: (deferred-emacs-package package) 

Generates a simple `use-package-configuration` for the _package_, with
**defer** set to `#t`.
  
Example Configuration
---------------------

```scheme
(eval-when (expand load eval)
  (enable-elisp-reader read-hash-extend #\e))

(user-home
  emacs-home-type
  (emacs-init-configuration
    (preamble '((setq ring-bell-function 'ignore))
    (use-packages
      (list
        (deferred-emacs-package emacs-typescript-mode)
	(use-package-configuration
	  (package emacs-helpful)
	  (diminish #t)
	  (defer #t)
	  (general '("C-h k" #e#'helpful-key
	             "C-h F" #e#'helpful-function
		     "C-h C" #e#'helpful-command)))
        (use-package-configuration
	  (package emacs-sly)
	  (defer #t)
	  (custom `(inferior-lisp-program ,(file-append sbcl "/bin/sbcl")))))))))
```
